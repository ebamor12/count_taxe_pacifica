package com.pacifica.calculetaxe.service.impl;

import com.pacifica.calculetaxe.common.utils.FileUtil;
import com.pacifica.calculetaxe.models.Invoice;
import com.pacifica.calculetaxe.models.InvoiceDetails;
import com.pacifica.calculetaxe.models.Order;
import com.pacifica.calculetaxe.service.InvoiceDetailsService;
import com.pacifica.calculetaxe.service.InvoiceService;
import com.pacifica.calculetaxe.service.TaxeCalculatorService;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static com.pacifica.calculetaxe.common.constant.InvoiceConstant.*;
import static com.pacifica.calculetaxe.common.constant.LogConstant.CREATE_INVOICE_FILE_LOG;
import static com.pacifica.calculetaxe.common.constant.LogConstant.FILE_EXIST_LOG;
import static com.pacifica.calculetaxe.common.constant.OrderLineConstant.A_ORDER;
import static com.pacifica.calculetaxe.common.constant.OrderLineConstant.EURO_IN_LINE_ORDER;


public class InvoiceServiceImpl implements InvoiceService {

    private static final Logger LOGGER = Logger.getLogger(InvoiceServiceImpl.class.getName());

    @Override
    public Invoice createInvoiceFromOrder(Order order, InvoiceDetailsService invoiceDetailsService, TaxeCalculatorService taxeCalculatorService) {
        Invoice invoice = new Invoice();
        List<InvoiceDetails> invoiceDetailsList = order.getOrderDetails()
                .stream()
                .map(orderDetails -> (invoiceDetailsService.createInvoiceDetail(orderDetails, taxeCalculatorService)))
                .collect(Collectors.toList());
        invoice.setInvoiceDetails(invoiceDetailsList);
        invoice.setTotalProductsTaxe(taxeCalculatorService.countTotalTax(order.getOrderDetails()));
        invoice.setTotaleProductsPrices(taxeCalculatorService.countTotalPriceTTC(order.getOrderDetails()));
        return invoice;
    }

    @Override
    public String createInvoiceFile(Invoice invoice) throws IOException {
        LOGGER.info(CREATE_INVOICE_FILE_LOG + invoice.getTotaleProductsPrices());
        File file = FileUtil.createNewFile(invoice);
        if (file.createNewFile()) {
            FileUtil.createTextInFile(file, this.createInvoiceText(invoice));
        } else {
            LOGGER.info(FILE_EXIST_LOG + file.getCanonicalPath());
        }
        return file.getCanonicalPath();
    }

    private String createInvoiceText(Invoice invoice) {
        List<String> invoiceProductLine = createInvoiceLinesText(invoice);
        String globalTaxe = TAXE_TOTAL + invoice.getTotalProductsTaxe() + EURO_IN_LINE_ORDER;
        String globalPrice = TOTAL_PRIX + invoice.getTotaleProductsPrices() + EURO_IN_LINE_ORDER;
        return String.join("\n", invoiceProductLine) + "\n\n" + globalTaxe + "\n" + globalPrice;
    }

    private List<String> createInvoiceLinesText(Invoice invoice) {
        List<String> invoiceProductLine = new ArrayList<>();
        if (invoice != null) {
            invoiceProductLine = invoice.getInvoiceDetails().stream()
                    .map(line -> createInvoiceLineText(line))
                    .collect(Collectors.toList());
        }
        return invoiceProductLine;
    }

    private String createInvoiceLineText(InvoiceDetails invoiceDetail) {
        return START_INVOICE_STRING + invoiceDetail.getQuantity() + SPACE_STRING + invoiceDetail.getProduct().getLibelle() + invoiceDetail.getImportLibelle() + A_ORDER
                + invoiceDetail.getProduct().getUnitPriceWithoutTaxe() + EURO_IN_INVOICE
                + invoiceDetail.getPriceTTC() + EURO_IN_LINE_ORDER;
    }


}
