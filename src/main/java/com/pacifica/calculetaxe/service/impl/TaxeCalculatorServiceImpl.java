package com.pacifica.calculetaxe.service.impl;

import com.pacifica.calculetaxe.common.utils.TaxeCalculatorUtil;
import com.pacifica.calculetaxe.models.OrderDetails;
import com.pacifica.calculetaxe.models.Product;
import com.pacifica.calculetaxe.service.TaxeCalculatorService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static com.pacifica.calculetaxe.common.constant.ValueConstant.FIVE_PERCENT;
import static com.pacifica.calculetaxe.common.constant.ValueConstant.TWO;

public class TaxeCalculatorServiceImpl implements TaxeCalculatorService {


    @Override
    public BigDecimal countProductPriceTTC(OrderDetails orderDetails) {
        return new BigDecimal(TaxeCalculatorUtil
                .approximityValue(BigDecimal.valueOf(orderDetails.getQuantity()).multiply(orderDetails.getProduct().getUnitPriceWithoutTaxe().add(coutUnitProductTaxe(orderDetails.getProduct()))))
                .setScale(TWO, RoundingMode.HALF_UP)
                .stripTrailingZeros()
                .toPlainString());
    }

    @Override
    public BigDecimal countTotalTax(List<OrderDetails> orderDetailsList) {
        return new BigDecimal(orderDetailsList
                .stream()
                .map(orderDetails -> countProductTaxe(orderDetails))
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .setScale(TWO, RoundingMode.HALF_UP)
                .stripTrailingZeros()
                .toPlainString());
    }

    @Override
    public BigDecimal countTotalPriceTTC(List<OrderDetails> orderDetailsList) {
        return new BigDecimal(orderDetailsList
                .stream()
                .map(orderDetails -> countProductPriceTTC(orderDetails))
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .stripTrailingZeros()
                .toPlainString());
    }

    private BigDecimal coutUnitProductTaxe(Product product) {
        BigDecimal importedProductTaxe = (product.isImported() ? TaxeCalculatorUtil.countTaxe(product.getUnitPriceWithoutTaxe(), FIVE_PERCENT) : BigDecimal.valueOf(0));
        BigDecimal otherTaxeOfProduct = TaxeCalculatorUtil.countTaxe(product.getUnitPriceWithoutTaxe(), product.getProductType().getTax());
        return importedProductTaxe.add(otherTaxeOfProduct);
    }

    private BigDecimal countProductTaxe(OrderDetails orderDetails) {
        BigDecimal totalPrice = orderDetails.getProduct().getUnitPriceWithoutTaxe().multiply(BigDecimal.valueOf(orderDetails.getQuantity()));
        return countProductPriceTTC(orderDetails).subtract(totalPrice).setScale(TWO, RoundingMode.HALF_UP);
    }
}
