package com.pacifica.calculetaxe.service;

import com.pacifica.calculetaxe.models.InvoiceDetails;
import com.pacifica.calculetaxe.models.OrderDetails;

public interface InvoiceDetailsService {

    InvoiceDetails createInvoiceDetail(OrderDetails orderDetail, TaxeCalculatorService taxeCalculatorService);
}

