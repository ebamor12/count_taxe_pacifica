package com.pacifica.calculetaxe.service;

import com.pacifica.calculetaxe.models.OrderDetails;

public interface OrderDetailsService {

    OrderDetails createOrderDetailFromOrderLine(String orderLine, ProductService productService);
}
