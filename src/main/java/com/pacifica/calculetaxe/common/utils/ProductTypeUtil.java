package com.pacifica.calculetaxe.common.utils;

import com.pacifica.calculetaxe.common.enumerations.ProductTypeEnum;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.pacifica.calculetaxe.common.constant.ProductNameConstant.*;

public final class ProductTypeUtil {

    public static Map<ProductTypeEnum, List<String>> getMatchingProductNames() {
        Map<ProductTypeEnum, List<String>> mapOfProductNameByType = new HashMap<>();
        mapOfProductNameByType.put(ProductTypeEnum.BOOK, Arrays.asList(PRODUCT_NAME_MANY_BOOKS, PRODUCT_NAME_BOOK));
        mapOfProductNameByType.put(ProductTypeEnum.FOOD, Arrays.asList(PRODUCT_NAME_MANY_CHOCOLATS, PRODUCT_NAME_CHOCOLAT, PRODUCT_NAME_CHOCOLATE_BARS, PRODUCT_NAME_CHOCOLATE_BOXES));
        mapOfProductNameByType.put(ProductTypeEnum.MEDICATION, Arrays.asList(PRODUCT_NAME_MANY_PILLS, PRODUCT_NAME_PILL, PRODUCT_NAME_PILL_BOXES));
        mapOfProductNameByType.put(ProductTypeEnum.OTHERS, Arrays.asList(PRODUCT_NAME_PERFUME, PRODUCT_NAME_CD, PRODUCT_NAME_PERFUME_BOTTLES, PRODUCT_NAME_PERFUME_BOTTLE));
        return mapOfProductNameByType;
    }

    private ProductTypeUtil() {
    }
}
