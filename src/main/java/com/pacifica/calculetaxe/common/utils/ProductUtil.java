package com.pacifica.calculetaxe.common.utils;

import com.pacifica.calculetaxe.models.Product;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.pacifica.calculetaxe.common.constant.OrderLineConstant.*;
import static com.pacifica.calculetaxe.common.constant.ValueConstant.TWO;

public final class ProductUtil {

    public static int getProductQuantityFromOderLine(String orderLine) {
        Matcher matcher = Pattern.compile("\\d+").matcher(orderLine);
        matcher.find();
        return Integer.valueOf(matcher.group());
    }

    public static String getProductLibelleFromOrderLine(Product product, String orderLine) {
        if (product.isImported()) {
            return (ProductUtil.substringLine(orderLine, String.valueOf(getProductQuantityFromOderLine(orderLine)), orderLine.substring(orderLine.indexOf(IMPORT_IN_LINE_ORDER), orderLine.indexOf(A_IN_LINE_ORDER)))).trim();
        } else {
            return (ProductUtil.substringLine(orderLine, String.valueOf(getProductQuantityFromOderLine(orderLine)), A_IN_LINE_ORDER)).trim();
        }
    }

    public static BigDecimal getProductUnitPriceFromOrderLine(String orderLine) {
        return new BigDecimal(ProductUtil.substringLine(orderLine, A_IN_LINE_ORDER, EURO_IN_LINE_ORDER));
    }

    public static String substringLine(String lineCommand, String startSubstring, String endSubstring) {
        int indexofStartSubstring = lineCommand.indexOf(startSubstring) + TWO;
        int indexofEndSubstring = lineCommand.indexOf(endSubstring);
        return lineCommand.substring(indexofStartSubstring, indexofEndSubstring);
    }

    private ProductUtil() {
    }
}
