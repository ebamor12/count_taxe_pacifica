package com.pacifica.calculetaxe.common.constant;

import java.math.BigDecimal;

public final class ValueConstant {

    public static final int ZERO_PERCENT                                 = 0;
    public static final int TEN_PERCENT                                  = 10;
    public static final int TWENTY_PERCENT                               = 20;
    public static final int FIVE_PERCENT                                 = 5;
    public static final BigDecimal FIVE_CENT                             = BigDecimal.valueOf(0.05);
    public static final int HUNDRED                                      = 100;
    public static final int TWO                                          = 2;

    private ValueConstant(){

    }
}
