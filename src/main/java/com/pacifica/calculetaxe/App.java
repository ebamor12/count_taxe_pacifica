package com.pacifica.calculetaxe;

import com.pacifica.calculetaxe.models.Invoice;
import com.pacifica.calculetaxe.service.*;
import com.pacifica.calculetaxe.service.impl.*;

import static com.pacifica.calculetaxe.common.constant.FileConstant.*;

import java.io.IOException;

public class App 
{
    public static void main( String[] args ) throws IOException {

        TaxeCalculatorService taxeCalculatorService = new TaxeCalculatorServiceImpl();
        ProductService productService = new ProductServiceImpl();
        OrderDetailsService orderDetailsService = new OrderDetailsServiceImpl();
        OrderService orderService = new OrderServiceImpl();
        InvoiceDetailsService invoiceDetailsService = new InvoiceDetailsServiceImpl();
        InvoiceService invoiceService = new InvoiceServiceImpl();

        Invoice invoice1 = invoiceService
                .createInvoiceFromOrder(orderService.createOrderFromFile(FIRST_COMMAND_FILE_PATH, orderDetailsService, productService),
                        invoiceDetailsService, taxeCalculatorService);
        invoiceService.createInvoiceFile(invoice1);

        Invoice invoice2 = invoiceService
                .createInvoiceFromOrder(orderService.createOrderFromFile(SECOND_COMMAND_FILE_PATH, orderDetailsService, productService),
                        invoiceDetailsService, taxeCalculatorService);
        invoiceService.createInvoiceFile(invoice2);

        Invoice invoice3 = invoiceService
                .createInvoiceFromOrder(orderService.createOrderFromFile(THIRD_COMMAND_FILE_PATH, orderDetailsService, productService),
                        invoiceDetailsService, taxeCalculatorService);
        invoiceService.createInvoiceFile(invoice3);

    }
}
