package com.pacifica.calculetaxe.models;

import java.math.BigDecimal;

public class InvoiceDetails extends OrderDetails{

    private BigDecimal priceTTC;

    public BigDecimal getPriceTTC() {
        return priceTTC;
    }

    public void setPriceTTC(BigDecimal priceTTC) {
        this.priceTTC = priceTTC;
    }
}
