package com.pacifica.calculetaxe;

import com.pacifica.calculetaxe.models.Invoice;
import com.pacifica.calculetaxe.service.*;
import com.pacifica.calculetaxe.service.impl.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static com.pacifica.calculetaxe.common.constant.FileConstant.*;

public class AcceptanceTest {

    InvoiceService invoiceService;
    OrderService orderService;
    TaxeCalculatorService taxeCalculatorService;
    ProductService productService;
    OrderDetailsService orderDetailsService;
    InvoiceDetailsService invoiceDetailsService;


    @Before
    public void setUp() {
        invoiceService = new InvoiceServiceImpl();
        orderService = new OrderServiceImpl();
        taxeCalculatorService = new TaxeCalculatorServiceImpl();
        productService = new ProductServiceImpl();
        orderDetailsService = new OrderDetailsServiceImpl();
        invoiceDetailsService = new InvoiceDetailsServiceImpl();
    }

    @Test
    public void should_create_invoice_file() throws IOException {
        Invoice invoice1 = invoiceService.createInvoiceFromOrder(orderService.createOrderFromFile(FIRST_COMMAND_FILE_PATH, orderDetailsService, productService),invoiceDetailsService, taxeCalculatorService);
        List<String> expectedLines = Files.lines(Paths.get(FIRST_EXPECTED_OUTPUT_FILE_PATH)).collect(Collectors.toList());
        List<String> lines = Files.lines(Paths.get(invoiceService.createInvoiceFile(invoice1)), Charset.forName(CHARSET)).collect(Collectors.toList());
        Assert.assertEquals(expectedLines, lines);
    }
}
