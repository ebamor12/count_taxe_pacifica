package com.pacifica.calculetaxe.services.impl;

import com.pacifica.calculetaxe.common.enumerations.ProductTypeEnum;
import com.pacifica.calculetaxe.models.Product;
import com.pacifica.calculetaxe.service.impl.ProductServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class ProductServiceImplTest {

    ProductServiceImpl productServiceImpl;

    @Before
    public void setUp() {
        productServiceImpl = new ProductServiceImpl();
    }

    @Test
    public void should_create_product_from_orderline() {
        String orderLine = "* 2 livres à 12.49€";
        Integer quantity = 2;
        Product expectedProduct = TestUtils.getBookProduct();
        Product product = productServiceImpl.createProductFromOrderLine(orderLine, quantity);
        Assert.assertEquals(product.getProductType(), expectedProduct.getProductType());
        Assert.assertEquals(product.getUnitPriceWithoutTaxe(), expectedProduct.getUnitPriceWithoutTaxe());
    }

    @Test
    public void should_create_product_with_Type_Other() {
        String orderLine = "* 2 casques à 12.49€";
        Integer quantity = 2;
        Product product = productServiceImpl.createProductFromOrderLine(orderLine, quantity);
        Assert.assertEquals(product.getProductType(), ProductTypeEnum.OTHERS);
    }
}
