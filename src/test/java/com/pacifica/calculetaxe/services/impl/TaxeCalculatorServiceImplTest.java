package com.pacifica.calculetaxe.services.impl;

import com.pacifica.calculetaxe.models.OrderDetails;
import com.pacifica.calculetaxe.service.TaxeCalculatorService;
import com.pacifica.calculetaxe.service.impl.TaxeCalculatorServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

public class TaxeCalculatorServiceImplTest {

    TaxeCalculatorService taxeCalculatorService;

    @Before
    public void setUp() {
        taxeCalculatorService = new TaxeCalculatorServiceImpl();
    }

    @Test
    public void should_calculate_product_priceTTC() {
        OrderDetails orderDetails = TestUtils.getFirstOrderLine();
        BigDecimal productPriceTTC = taxeCalculatorService.countProductPriceTTC(orderDetails);
        BigDecimal expectedProductPriceTTC = BigDecimal.valueOf(27.5);
        Assert.assertEquals(productPriceTTC, expectedProductPriceTTC);

    }

    @Test
    public void should_calculate_product_totale_priceTTC() {
        List<OrderDetails> orderDetailsList = TestUtils.getOrder().getOrderDetails();
        BigDecimal totaleProductPriceTTC = taxeCalculatorService.countTotalPriceTTC(orderDetailsList);
        BigDecimal expectedProductPriceTTC = BigDecimal.valueOf(48.05);
        Assert.assertEquals(totaleProductPriceTTC, expectedProductPriceTTC);
    }

    @Test
    public void should_calculate_product_totale_taxe() {
        List<OrderDetails> orderDetailsList = TestUtils.getOrder().getOrderDetails();
        BigDecimal totaleTaxe = taxeCalculatorService.countTotalTax(orderDetailsList);
        BigDecimal expectedTotaleTaxe = BigDecimal.valueOf(5.53);
        Assert.assertEquals(totaleTaxe, expectedTotaleTaxe);
    }
}
