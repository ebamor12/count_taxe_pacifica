package com.pacifica.calculetaxe.services.impl;

import com.pacifica.calculetaxe.models.Invoice;
import com.pacifica.calculetaxe.models.Order;
import com.pacifica.calculetaxe.service.InvoiceDetailsService;
import com.pacifica.calculetaxe.service.TaxeCalculatorService;
import com.pacifica.calculetaxe.service.impl.InvoiceDetailsServiceImpl;
import com.pacifica.calculetaxe.service.impl.InvoiceServiceImpl;
import com.pacifica.calculetaxe.service.impl.TaxeCalculatorServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class InvoiceServiceImplTest {
    InvoiceServiceImpl invoiceServiceImpl;

    @Before
    public void setUp() {
        invoiceServiceImpl = new InvoiceServiceImpl();
    }

    @Test
    public void should_create_invoice_from_order() {
        Order order = TestUtils.getOrder();
        InvoiceDetailsService invoiceDetailsService = new InvoiceDetailsServiceImpl();
        TaxeCalculatorService taxeCalculatorService = new TaxeCalculatorServiceImpl();
        Invoice expectedInvoice = TestUtils.getInvoice();
        Invoice invoice = invoiceServiceImpl.createInvoiceFromOrder(order, invoiceDetailsService, taxeCalculatorService);
        Assert.assertNotNull(invoice);
        Assert.assertEquals(invoice.getTotaleProductsPrices(), expectedInvoice.getTotaleProductsPrices());
        Assert.assertEquals(invoice.getTotalProductsTaxe(), expectedInvoice.getTotalProductsTaxe());
        Assert.assertEquals(invoice.getInvoiceDetails().size(), expectedInvoice.getInvoiceDetails().size());
    }

}
