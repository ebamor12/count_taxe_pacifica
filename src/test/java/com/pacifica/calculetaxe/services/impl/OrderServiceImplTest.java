package com.pacifica.calculetaxe.services.impl;

import com.pacifica.calculetaxe.models.Order;
import com.pacifica.calculetaxe.service.OrderDetailsService;
import com.pacifica.calculetaxe.service.OrderService;
import com.pacifica.calculetaxe.service.impl.OrderDetailsServiceImpl;
import com.pacifica.calculetaxe.service.impl.OrderServiceImpl;
import com.pacifica.calculetaxe.service.impl.ProductServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static com.pacifica.calculetaxe.common.constant.FileConstant.FIRST_COMMAND_FILE_PATH;

public class OrderServiceImplTest {

    OrderService orderService;

    @Before
    public void setUp() {
        orderService = new OrderServiceImpl();
    }

    @Test
    public void should_create_order_from_file() throws IOException {
        ProductServiceImpl productService = new ProductServiceImpl();
        OrderDetailsService orderDetailsService = new OrderDetailsServiceImpl();
        Order order = orderService.createOrderFromFile(FIRST_COMMAND_FILE_PATH, orderDetailsService, productService);
        Order expectedOrder = TestUtils.getOrder();
        Assert.assertEquals(order.getOrderDetails().size(), expectedOrder.getOrderDetails().size());
    }
}
